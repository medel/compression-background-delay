#include "util_debug.h"
#include "util_libcamera.h"
#include "util_camera_capture.h"

#include <climits>
#include <fcntl.h>
#include <iostream>
#include <poll.h>
#include <queue>
#include <sys/mman.h>
#include <unistd.h>

#include <libcamera/libcamera.h>

#define pr_err(msg) std::cerr << msg << std::endl
#define pr_info(msg) std::cout << msg << std::endl

#define container_of(ptr, type, member) \
	(type *)((char *)(ptr) - offsetof(type, member))

#define to_libcamera_source(dev) container_of(dev, struct libcamera_source, cap_dev)

using namespace libcamera;

struct libcamera_source {
        capture_dev_t cap_dev;

        std::unique_ptr<CameraManager> cm;
        std::shared_ptr<Camera> camera;
        std::unique_ptr<CameraConfiguration> config;

        FrameBufferAllocator *allocator;
        std::vector<std::unique_ptr<Request>> requests;
        std::queue<Request *> completed_requests;
        int pfds[2];

        void requestComplete(Request *request);
};

void libcamera_source::requestComplete(Request *request)
{
        if (request->status() == Request::RequestCancelled)
                return;

        completed_requests.push(request);

        /* (void)! to quieten the -Wunused_result warning */
        (void)!write(pfds[1], "x", 1);
}

capture_frame_t *libcamera_acquire_capture_frame(capture_dev_t *cap_dev)
{
        struct libcamera_source *src = to_libcamera_source(cap_dev);
        struct pollfd fds[1] = { 0 };
        Request *request;
        char buf;
        int ret;

        fds[0].fd = src->pfds[0];
        fds[0].events = POLLIN;

        ret = poll(fds, 1, -1);
        if (ret < 0)
                return NULL;

        ret = read(src->pfds[0], &buf, 1);

        if (src->completed_requests.empty())
                return NULL;

        request = src->completed_requests.front();
        src->completed_requests.pop();

        return &src->cap_dev.stream.frames[request->cookie()];
}

void libcamera_release_capture_frame(capture_dev_t *cap_dev, capture_frame_t *cap_frame)
{
        struct libcamera_source *src = to_libcamera_source(cap_dev);

	for (std::unique_ptr<Request> &r : src->requests) {
		if (r->cookie() == cap_frame->index) {
			r->reuse(Request::ReuseBuffers);
			src->camera->queueRequest(r.get());

			break;
		}
	}
}

static int libcamera_init_stream(struct libcamera_source *src, int width, int height)
{
        capture_stream_t *cap_stream = &src->cap_dev.stream;
        struct v4l2_pix_format *fmt = &cap_stream->format.fmt.pix;
        StreamConfiguration &streamConfig = src->config->at(0);
        int ret;

        streamConfig.size.width = width;
        streamConfig.size.height = height;
        streamConfig.pixelFormat = PixelFormat(V4L2_PIX_FMT_YUYV);
        streamConfig.bufferCount = 4;

        src->config->validate();

        if (streamConfig.pixelFormat.fourcc() != v4l2_fourcc('Y', 'U', 'Y', 'V')) {
                pr_err("Requested format unsupported");
                return 1;
        }

        ret = src->camera->configure(src->config.get());
        if (ret) {
                pr_err("failed to configure the camera");
                return ret;
        }

        cap_stream->memtype = V4L2_MEMORY_MMAP;
        cap_stream->bufcount = streamConfig.bufferCount;
        cap_stream->format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt->width = streamConfig.size.width;
        fmt->height = streamConfig.size.height;
        fmt->pixelformat = streamConfig.pixelFormat.fourcc();
        fmt->field = V4L2_FIELD_ANY;
        fmt->sizeimage = fmt->width * fmt->height * 2;

        return 0;
}

static int libcamera_alloc_buffers(struct libcamera_source *src)
{
        Stream *stream = src->config->at(0).stream();
        capture_frame_t *capture_frames;
        FrameBufferAllocator *allocator;
        unsigned int nbufs;
        int ret;

        allocator = new FrameBufferAllocator(src->camera);

        ret = allocator->allocate(stream);
        if (ret < 0) {
                pr_err("failed to allocate buffers");
                return ret;
        }

        src->allocator = allocator;
        nbufs = src->cap_dev.stream.bufcount;

        capture_frames = (capture_frame_t *)calloc(nbufs, sizeof(capture_frame_t));
        if (!capture_frames) {
                pr_err("failed to allocate memory for capture frames");
                return -ENOMEM;
        }

        src->cap_dev.stream.frames = capture_frames;
        const std::vector<std::unique_ptr<FrameBuffer>> &buffers = allocator->buffers(stream);
	
        for (unsigned int i = 0; i < nbufs; i++) {
                const std::unique_ptr<FrameBuffer> &buffer = buffers[i];
                const FrameBuffer::Plane &plane = buffer->planes()[0];
                capture_frame_t *frame = &capture_frames[i];

                frame->vaddr = mmap (NULL, plane.length, PROT_WRITE|PROT_READ, 
                                        MAP_SHARED, plane.fd.get(), 0);
                
                frame->v4l_buf.index  = i;
                frame->index          = i;
                frame->v4l_buf.memory = V4L2_MEMORY_MMAP;
        }

        return 0;
}
extern "C" {
int libcamera_start_capture(capture_dev_t *cap_dev)
{
        struct libcamera_source *src = to_libcamera_source(cap_dev);
	Stream *stream = src->config->at(0).stream();
	int ret;

	const std::vector<std::unique_ptr<FrameBuffer>> &buffers = src->allocator->buffers(stream);

	for (unsigned int i = 0; i < buffers.size(); ++i) {
		std::unique_ptr<Request> request = src->camera->createRequest(i);
		if (!request) {
			std::cerr << "failed to create request" << std::endl;
			return -ENOMEM;
		}

		const std::unique_ptr<FrameBuffer> &buffer = buffers[i];
		ret = request->addBuffer(stream, buffer.get());
		if (ret < 0) {
			std::cerr << "failed to set buffer for request" << std::endl;
			return ret;
		}

		src->requests.push_back(std::move(request));
	}

	ret = src->camera->start();
	if (ret) {
		std::cerr << "failed to start camera" << std::endl;
		return ret;
	}

	for (std::unique_ptr<Request> &request : src->requests) {
		ret = src->camera->queueRequest(request.get());
		if (ret) {
			std::cerr << "failed to queue request" << std::endl;
			src->camera->stop();
			return ret;
		}
	}

	return 0;
}
}
void libcamera_get_capture_format(capture_dev_t *cap_dev, int *cap_w, int *cap_h,
                                  unsigned int *cap_fmt)
{
        struct v4l2_pix_format fmt = cap_dev->stream.format.fmt.pix;

        *cap_w = fmt.width;
        *cap_h = fmt.height;
        *cap_fmt = fmt.pixelformat;
}

extern "C" capture_dev_t *libcamera_init_camera(long unsigned int camera_index,
                                                int width, int height)
{
        struct libcamera_source *src;
        int ret;

        src = new libcamera_source;

        ret = pipe2(src->pfds, O_NONBLOCK);
        if (ret) {
                pr_err("failed to create pipe");
                goto err_free_src;
        }

        if (camera_index < 0)
                return NULL;

        src->cm = std::make_unique<CameraManager>();
        src->cm->start();

        if (src->cm->cameras().empty()) {
                pr_err("No cameras were identified on the system");
                return NULL;
        }

        if (camera_index >= src->cm->cameras().size()) {
                pr_err("No camera at index " << camera_index);
                goto err_stop_cm;
        }

        src->camera = src->cm->cameras()[camera_index];
        ret = src->camera->acquire();
        if (ret) {
                pr_err("Failed to acquire camera");
                goto err_stop_cm;
        }

        pr_info("Using camera " << src->camera->id());

        src->config = src->camera->generateConfiguration( { StreamRole::VideoRecording });
        if (!src->config) {
                pr_err("Failed to generate camera config");
                goto err_release_camera;
        }

        src->camera->requestCompleted.connect(src, &libcamera_source::requestComplete);

        ret = libcamera_init_stream(src, width, height);
        if (ret)
                goto err_disconnect_callback;

        ret = libcamera_alloc_buffers(src);
        if (ret)
                goto err_disconnect_callback;

        return &src->cap_dev;

err_disconnect_callback:
        src->camera->requestCompleted.disconnect(src);
err_release_camera:
        src->camera->release();
err_stop_cm:
        src->cm->stop();
err_free_src:
        delete src;

        return NULL;
}
